import logo from './logo.svg';
import './App.css';
import Halaman2 from './Halaman2'
import Halaman3 from './Halaman3';
import Component from './Component';
import {Link, BrowserRouter as Router, Switch, Route} from "react-router-dom";

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h1>
          M Ihsan Hafizhin
        </h1>
       <Router> 
         <Route path='/welcome' component={Component}/>
         <Route path='/halaman2' component={Halaman2} />
         <Route path='/halaman3' component={Halaman3} />
         <Link to='/welcome'>
              <p style={{color:'white'}}>Pergi Ke halaman welcome</p>
          </Link>
          <Link to='/halaman2'>
              <p style={{color:'white'}}>Pergi Ke halaman 2</p>
          </Link>
          <Link to='/halaman3'>
              <p style={{color:'white'}}>Pergi Ke halaman 3</p>
          </Link>
       </Router>
      </header>
    </div>
  );
}

export default App;
