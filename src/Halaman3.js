import logo from './logo.svg';
import React from 'react';
import './App.css';
import App from './App';
import {Link, BrowserRouter as Router} from "react-router-dom";

function Halaman3(){
    return(
    <div className="App">
      <header className="App-header">
          <h1>Ini Adalah Halaman ke 3</h1>
       <p>Contoh membuat layout</p>
      </header>
    </div>
    )
}

export default Halaman3;