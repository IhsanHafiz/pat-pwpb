import React from 'react';

function Welcome(props) {
    return <h1>Halo, {props.name}</h1>;
  }

  function Component(){
      return(
          <div>
             <header className="App-header">
                <Welcome name="Pak Jahid" />
            </header>
          </div>
      )
  }

  export default Component;